# Payum SEB banklink module by Omnibuy - usage example with Symfony

## Introduction
The purpose of this example project is to show how to use the `omnibuy/payum-seb` module correctly with 
Symfony and templates. Since the SEB banklink integration requires a client side redirect, then a HTML form needs to 
be rendered and redirected using JavaScript.

## Usage

- `composer install`
- Set the config parameters in `.env`
- `php bin/console doctrine:migrations:migrate`
- `php bin/console server:start`
- `ngrok http 8000`
- Open the Ngrok URL in the browser

## License

Project is released under the [MIT License](LICENSE).
