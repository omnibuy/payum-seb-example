<?php

namespace App\Controller;

use Payum\Core\Payum;
use Payum\Core\Request\GetHumanStatus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    public function index()
    {
        return $this->render('index.html.twig', ['number' => 1]);
    }

    public function startPayment(Payum $payum)
    {
        $storage = $payum->getStorage('App\Entity\Payment');

        $payment = $storage->create();
        $payment->setNumber(uniqid());
        $payment->setCurrencyCode('EUR');
        $payment->setTotalAmount(123); // 1.23 EUR
        $payment->setDescription('A description');
        $payment->setClientId('anId');
        $payment->setClientEmail('foo@example.com');

        $storage->update($payment);

        $gatewayName = 'seb';
        $captureToken = $payum->getTokenFactory()->createCaptureToken(
            $gatewayName,
            $payment,
            'thank_you'
        );

        return $this->redirect($captureToken->getTargetUrl());
    }

    public function thankYou(Request $request, Payum $payum)
    {
        $token = $payum->getHttpRequestVerifier()->verify($request);
        $gateway = $payum->getGateway($token->getGatewayName());
        $gateway->execute($status = new GetHumanStatus($token));

        return $this->render('thank_you.html.twig', ['status' => $status]);
    }
}
